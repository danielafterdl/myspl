<?php

require_once dirname(__FILE__) . '/../vendor/autoload.php';

use MySpl\RndPct;

class RndPctTest extends PHPUnit_Framework_TestCase
{
    protected $_rndpct;

    public function setup()
    {
        $this->_rndpct = new RndPct;
    }

    public function testPercentage()
    {
        $weights = array(40, 30, 20, 10);
        shuffle($weights);

        $this->_percentageResults($this->_rndpct, $weights);
    }

    public function testGranularity()
    {
        $rndpct = new RndPct;

        $weights = range(1, 100);
        shuffle($weights);

        $this->markTestIncomplete('This test is incomplete.');

        $this->_percentageResults($this->_rndpct, $weights);
    }

    public function testCount()
    {
        $rndpct = $this->_rndpct;

        $weights = array(40, 30, 20, 10);
        shuffle($weights);

        foreach ($weights as $w) {
            $this->assertEquals(
                $rndpct->count($w),
                0
            );

            $rndpct->add($w, $w);
            $this->assertEquals(
                $rndpct->count($w),
                1
            );

            $rndpct->add($w, $w*2);
            $this->assertEquals(
                $rndpct->count($w),
                2
            );
        }
    }

    protected function _percentageResults(RndPct $rndpct, array $weights, $loops = 100000)
    {
        $hits = array();
        foreach ($weights as $w) {
            $rndpct->add($w, $w);
            $hits[$w] = 0;
        }

        $i = 0;
        while ($i++ < $loops) {
            $r = $rndpct->get(null, -1);
            $hits[$r] += 1;
        }

        $total_hits = array_sum($hits);
        $pct_1 = ceil($total_hits / 100);

        foreach ($hits as $weight => $weight_hits) {
            $this->assertTrue(
                $weight >= 0,
                sprintf(
                    'Weight should be positive, got [%d]',
                    $weight
                )
            );

            $expected  = $weight * $pct_1;
            $diff      = abs($weight_hits - $expected);
            $tolerance = $pct_1;

            $this->assertTrue(
                $diff < $tolerance,
                sprintf(
                    'Too big difference: expected max [%d], but got [%s] in weigth [%d].',
                    $tolerance,
                    $diff,
                    $weight
                )
            );
        }
    }
}
