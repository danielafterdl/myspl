<?php

namespace MySpl;

class RndPct
{
    /**
     * Local data cache array.
     *
     * @var array
     */
    protected $_cache = array();

    /**
     * Array of data to randomize with weights as keys.
     *
     * @var array
     */
    protected $_data = array();

    /**
     * Returns array of ordered available weights to randomize.
     *
     * @return array
     */
    public function weights()
    {
        if (false === ($weights = $this->_getCache(__METHOD__))) {
            $weights = array_keys($this->_data);
            arsort($weights);
            $this->_setCache(__METHOD__, $weights);
        }

        return $weights;
    }

    /**
     * Returns number of items in weigth.
     *
     * @param number $weight
     * @return int
     */
    public function count($weight)
    {
        return isset($this->_data[$weight]) ? count($this->_data[$weight]) : 0;
    }

    /**
     * Returns the sum of all available weigths.
     *
     * @return number
     */
    public function sum()
    {
        if (false === ($sum = $this->_getCache(__METHOD__))) {
            $sum = array_sum($this->weights());
            $this->_setCache(__METHOD__, $sum);
        }

        return $sum;
    }

    /**
     * Adds $item to the list of items selectable for weight $weight.
     *
     * @param number $weight
     * @param mixed  $item
     * @return self
     */
    public function add($weight, $item)
    {
        if (!isset($this->_data[$weight])) {
            $this->_data[$weight] = array();
        }

        $this->_data[$weight][] = $item;

        // Clear local cache after modifications...
        $this->_clearCache();

        return $this;
    }

    /**
     * Returns one item from the available weights.
     *
     * The item is chosen randomly from those that share the same weight, and the weigth is chosen according to
     * its probability. For example if two weigths exist (A=70 and B=30), then 70% of the times A will be chosen and
     * 30% of the times B.
     *
     * If $filter is provided then it should be a callable that accepts one parameter, an item, and returns a boolean
     * indicating if the item is acceptable or not. Only accepted items are elegible for selection. If the weight has
     * no acceptable items then the weigth is "skipped" and the weight selection continues.
     *
     * The returned item is selected randomly from the chosen weight's items (after filtering, see above), giving
     * equal probabilities to each item. Thus a weigth that gives it 100% probability with 3 items would produce each
     * item 33% of the times, or a weight with 50% probability with 2 items would yield each item 25% of the times.
     *
     * @param callable $filter
     * @param number   $weigth
     * @param mixed    $default
     * @return mixed
     */
    public function get($filter = null, $default = null)
    {
        // If filter is not callable, then omit it...
        if (!($filter && is_callable($filter))) {
            $filter = null;
        }

        // Throw the dice...
        $rand = mt_rand(0, $this->sum());

        // Keep running weight sum, start from zero
        $sum = 0;

        // For every weigth...
        foreach ($this->weights() as $weight) {
            // Increase running weight sum
            $sum += $weight;

            // Is it a percentage-wise hit?
            if ($rand <= $sum) {
                // Do we have results after filtering?
                if (
                    $available = ($filter) ? array_filter(
                        $this->_data[$weight],
                        $filter
                    ) : $this->_data[$weight]
                ) {
                    // We have a winner!
                    $item = $available[array_rand($available)];
                    break;
                }
            }
        }

        return isset($item) ? $item : $default;
    }

    /**
     * Store in local cache $value under $key.
     *
     * @param string $key
     * @param mixed $value
     * @return self
     */
    protected function _setCache($key, $value)
    {
        $this->_cache[$key] = $value;

        return $this;
    }

    /**
     * Returns value of local cache's $key or FALSE if not set.
     *
     * @param string $key
     * @return mixed|false
     */
    protected function _getCache($key)
    {
        return isset($this->_cache[$key]) ? $this->_cache[$key] : false;
    }

    /**
     * Clears the local cache.
     *
     * @return self
     */
    protected function _clearCache()
    {
        $this->_cache = array();

        return $this;
    }
}
